package com.stethoscopemodule;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.le.ScanResult;
import android.content.ServiceConnection;

import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;
import com.kl.minttisdk.ble.BleUtils;
import com.kl.minttisdk.ble.callback.ScanResultCallback;
import com.kl.minttisdk.ble.callback.ScanResultCallback;
import com.kl.minttisdk.ble.callback.BleDataCallback;
import com.kl.com.kl.minttisdkdemo.*;
import java.util.ArrayList;
import java.util.List;

public class StethoscopeModule extends ReactContextBaseJavaModule implements BleUtils {
    public LeDeviceListAdapter leDeviceListAdapter;

    private static ReactApplicationContext reactContext;

    StethoscopeModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    public void startScanBLE(){
        Log.d("Stethoscope: ", "Start scanning devices");
        BleUtils.getInstance().startScan(new ScanResultCallback(){
            @Override
            public void onScanResult(int i, ScanResult scanResult) {
                BluetoothDevice bluetoothDevice = scanResult.getDevice();
                Log.e("MinttiStethoscope", "onScanResult: 发现设备并连接" );
                BleDevice bleDevice = new BleDevice(bluetoothDevice);
                BleUtils.getInstance().stopScan();
                BleUtils.getInstance().connect(bleDevice,statusCallBack);
                WritableMap params = Arguments.createMap();
                params.putString("Scan result", String.valueOf(i));
                sendEvent(reactContext, "OnScanResultEvent", params);

            }

            @Override
            public void onBatchScanResults(List<ScanResult> list) {
                Log.d("Batch Scan Result","%" + list);
            }

            @Override
            public void onScanFailed(int i) {

                Log.d("Failed scan","%" + i);
            }
        });
    }

    @ReactMethod
    public void dataReceive(){
        BleUtils.getInstance().setBleDataCallback(new BleDataCallback(){
            @Override
            public void onSpkData(short[] spkData) {
                Log.d("Speak Data","%" + spkData);
            }

            @Override
            public void onMicData(short[] micData) {
                Log.d("Cardiopulmonary Data","%" + micData);
            }

            @Override
            public void onResultData(short[] resultData) {
                Log.d("Result Data","%" + resultData);
            }
            @Override
            public void onPowerNotify ( int power){
                BleUtils.getInstance().readDevicePower();
                Log.d("MinttiStethoscope Power","%" + power);

                //Mapping objects which are sended to JS
                WritableMap params = Arguments.createMap();
                params.putString("power state", String.valueOf(power));
                sendEvent(reactContext, "OnPowerNotifyEvent", params);

            }

            @Override
            public void onModeSwitch (EchoMode echoMode){
                BleUtils.getInstance().setEchoModeSwitch(EchoMode.MODE_DIAPHRAGM_ECHO);
                Log.d("MinttiStethoscope Mode", "onModeSwitch = " + echoMode.name());

            }

            @Override
            public void onHeartRate ( final int heartRate){
                Log.d("MinttiStethoscope HR", "heart rate: " + heartRate);
                //send heart rate datas to JS
                WritableMap params = Arguments.createMap();
                params.putString("Heart Rate", String.valueOf(heartRate));
                sendEvent(reactContext, "OnHeartRateEvent", params);

            }

            @Override
            public void onError (String s){
                Log.d("MinttiStethoscope Error", s);
            }

        });
    }

    @ReactMethod
    private void stopScanBLE() {
        Toast.makeText(reactContext,"Scanning is stopping !",Toast.LENGTH_LONG).show();
        if (BleUtils.getInstance().isScanning()) {
            BleUtils.getInstance().stopScan();
        }
    }

    @ReactMethod
    public void addDevice(BleDevice dev) {
        ArrayList<BleDevice> mLeDevices = new ArrayList<BleDevice>();
        LayoutInflater mInflator;
        int i = 0;
        int listSize = mLeDevices.size();
        for (i = 0; i < listSize; i++) {
            if (mLeDevices.get(i).getDevice().equals(dev.getDevice())) {
                mLeDevices.get(i).setRssi(dev.getRssi());
                break;
            }
        }

        if (i >= listSize) {
            mLeDevices.add(dev);
        }

    }
   @ReactMethod
    public void createFile() {
        BleUtils.getInstance().enableAudioDataNotify();
    }

    @ReactMethod
    public void closeFile() {
        BleUtils.getInstance().disableAudioDataNotify();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @ReactMethod
    public void connectDevice() {
        BleUtils.getInstance().startScan(startScanBLE);
    }

    @ReactMethod
    public void disconnect() {
        BleUtils.getInstance().disconnect();
    }

    @ReactMethod
    public void statusCallBack(){
        BleUtils.getInstance().connect(new BleConnectStatusCallback(){
            @Override
            public void onStartConnect() {

            }
            @Override
            public void onConnectFail(BleDevice bleDevice, String s) {
                Toast.makeText(reactContext,"Connection is failed",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onConnectSuccess(BleDevice bleDevice, BluetoothGatt bluetoothGatt, int status) {
                Toast.makeText(reactContext,"Connection is successful",Toast.LENGTH_LONG).show();
                Log.d("MinttiStethoscope", "onConnectSuccess: " + status);
                Log.d("MinttiStethoscope", "onConnectSuccess MAC: " + bleDevice.getMac());
                WritableMap params = Arguments.createMap();
                params.putString("Connected device status", String.valueOf(status));
                sendEvent(reactContext, "OnConnectSuccessEvent", params);


            }

            @Override
            public void onUpdateParamsSuccess() {
                BleUtils.getInstance().readDevicePower();
            }

            @Override
            public void onUpdateParamsFail() {
                Toast.makeText(reactContext,"Updating Failed",Toast.LENGTH_LONG).show();

            }

            @Override
            public void onDisConnected(boolean isActive, BluetoothDevice bluetoothDevice, BluetoothGatt bluetoothGatt, int status) {
                if (!isActive){
                    Toast.makeText(reactContext,"Disconnected",Toast.LENGTH_LONG).show();

                }
                Log.e("MinttiStethoscope", "onDisConnected: "+isActive);

            }
        });
    }
    @ReactMethod
    public void getDeviceID(Promise promise){
        try{
            String android_id= Settings.Secure.getString(reactContext.getContentResolver(), Settings.Secure.ANDROID_ID);
            promise.resolve(android_id);
        }
        catch(Exception e){
            promise.reject("Error",e);

        }

    }

    @NonNull
    @Override
    public String getName() {
        return "StethoscopeModule";
    }
}

